package recorder.server;

import recorder.common.listener.KeyExitListener;

public class AppServer {
	
	public static void main(String[] args) {
		CliParser parser = new CliParser(args);
		parser.parse();
		ServerConnectionService serverService = new ServerConnectionService();
		KeyExitListener.addExitKeyListener(serverService);
		serverService.receiveStreams();
	}
	
}
