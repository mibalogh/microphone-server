package recorder.server;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

public class CliParser {

	private static final Logger LOGGER = Logger.getLogger(CliParser.class);

	private String[] mArgs;

	private Options mOptions = new Options();

	public CliParser(String[] args) {
		mArgs = args;

		mOptions.addOption("p", "port", true, "server's port, default 9898");
		mOptions.addOption("h", "help", false, "shows help");
	}
	
	public void parse() {
		CommandLineParser parser = new DefaultParser();
		try {
			CommandLine cmd = parser.parse(mOptions, mArgs);
			
			if(cmd.hasOption("h")) {
				printHelp();
			}
			
			if(cmd.hasOption("p")) {
				String value = cmd.getOptionValue("p");
				try {
					Integer port = Integer.valueOf(value);
					Configuration.setPort(port);
					LOGGER.info("Server's port set to " + value);
				} catch (NumberFormatException e) {
					LOGGER.info("Wrong port number. Using default value: " + Configuration.getPort());
				}
			}
			
		} catch (ParseException e) {
			LOGGER.error("Failed to parse command line arguments.", e);
		}
	}

	
	private void printHelp() {
		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("server", mOptions);
		System.exit(0);
	}
}
