package recorder.server;

public class Configuration {

	private static int mPort = 9898;
	
	private static String mUserHome = System.getProperty("user.home");
	
	private static final byte[] MAGIC_NUMBER = new byte[] {0x12, 0x34};
	
	public static int getPort() {
		return mPort;
	}

	public static void setPort(int port) {
		Configuration.mPort = port;
	}
	
	public static String getUserHome() {
		return mUserHome;
	}
	
	public static void setUserHome(String userHome) {
		Configuration.mUserHome = userHome;
	}

	public static byte[] getMagicNumber() {
		return MAGIC_NUMBER;
	}
	
}
