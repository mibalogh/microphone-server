package recorder.server.file;

import java.nio.ByteBuffer;
import java.util.Arrays;

import recorder.common.exception.DataTransferException;
import recorder.common.properties.AudioFormatPropertiesReader;
import recorder.server.Configuration;

public class FileDataDecoder {
	
	private final static int[] MAGIC_NUMBER_INDEX = new int[] {0, 2};
	
	private final static int[] PACKET_SIZE_INDEX = new int[] {2, 4};
	
	private final static int[] TIMESTAMP_INDEX = new int[] {4, 12};
	
	private final static int[] FORMAT_INDEX = new int[] {12, 14};
	
	private final static int FILE_CONTENT_INDEX = 14;
	
	private final static String SLASH = "/";
	
	private final static String DOT = ".";

	private byte[] mData;
	
	public FileDataDecoder(byte[] data) throws DataTransferException {
		checkLength(data);
		checkMagicNumber(data);
		checkPacketSize(data);
		mData = data;
	}
	
	public String getFileName() {
		return decodeRecordingProperties() + SLASH + decodeTimestamp() + DOT + decodeExtension();
	}
	
	public byte[] getFileContent() {
		return Arrays.copyOfRange(mData, FILE_CONTENT_INDEX, mData.length);
	}
	
	
	public byte[] getServerResponse() {
		byte[] response = new byte[MAGIC_NUMBER_INDEX[1] - MAGIC_NUMBER_INDEX[0] + 1 + Long.BYTES];
		System.arraycopy(mData, MAGIC_NUMBER_INDEX[0], response, MAGIC_NUMBER_INDEX[0], MAGIC_NUMBER_INDEX[1] - MAGIC_NUMBER_INDEX[0]);
		System.arraycopy(mData, TIMESTAMP_INDEX[0], response, MAGIC_NUMBER_INDEX[1] - MAGIC_NUMBER_INDEX[0], Long.BYTES);
		return response;
	}

	private String decodeTimestamp() {
		byte[] timestampEncoded = Arrays.copyOfRange(mData, TIMESTAMP_INDEX[0], TIMESTAMP_INDEX[1]);
		ByteBuffer buffer = ByteBuffer.wrap(timestampEncoded);
		return String.valueOf(buffer.getLong());
	}
	
	private String decodeRecordingProperties() {
		byte[] audioFormatEncoded = Arrays.copyOfRange(mData, FORMAT_INDEX[0], FORMAT_INDEX[1]);
		ByteBuffer buffer = ByteBuffer.wrap(audioFormatEncoded);
		Short formatNumber = buffer.getShort();
		return AudioFormatPropertiesReader.getKey(formatNumber).toUpperCase();
	}
	
	private String decodeExtension() {
		return "pcm";
	}
	
	private void checkMagicNumber(byte[] data) throws DataTransferException {
		if(!Arrays.equals(Configuration.getMagicNumber(), Arrays.copyOfRange(data, MAGIC_NUMBER_INDEX[0], MAGIC_NUMBER_INDEX[1]))) {
			throw new DataTransferException("Magic numbers do not match.");
		}
	}
	
	private void checkPacketSize(byte[] data) throws DataTransferException {
		byte[] packetSizeEncoded = Arrays.copyOfRange(data, PACKET_SIZE_INDEX[0], PACKET_SIZE_INDEX[1]);
		ByteBuffer buffer = ByteBuffer.wrap(packetSizeEncoded);
		if(buffer.getShort() != (short) (data.length - Configuration.getMagicNumber().length - Short.BYTES) ) {
			throw new DataTransferException("Received data does not match the encoded size.");
		}
	}
	
	private void checkLength(byte[] data) throws DataTransferException {
		if(data.length <= FORMAT_INDEX[1]) {
			throw new DataTransferException("Received bytearray is too short.");
		}
	}
}
