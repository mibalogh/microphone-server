package recorder.server;

import org.apache.log4j.Logger;

import recorder.common.exception.DataTransferException;
import recorder.common.listener.IControlledService;
import recorder.server.file.FileDataDecoder;
import recorder.server.network.ConnectionHandlerFactory;
import recorder.server.network.IConnectionHandler;
import recorder.server.network.IReceiver;

public class ServerConnectionService implements IControlledService {

	private final static Logger LOGGER = Logger.getLogger(ServerConnectionService.class);

	private boolean mIsRunning = true;

	private IConnectionHandler mConnectionHandler;

	@Override
	public boolean isRunning() {
		return mIsRunning;
	}

	@Override
	public void setRunning(boolean isRunning) {
		this.mIsRunning = isRunning;

	}

	@Override
	public void shutdown() {
		mConnectionHandler.closeConnection();
	}

	public void receiveStreams() {

		mConnectionHandler = ConnectionHandlerFactory.getConnectionHandler();
		while (mIsRunning) {
			IReceiver dataReceiver = mConnectionHandler.listenForConnection();
			if (dataReceiver != null) {
				new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							byte[] dataFromClient = dataReceiver.readData();
							FileDataDecoder decoder;
							decoder = new FileDataDecoder(dataFromClient);
							dataReceiver.saveDataToFile(decoder.getFileContent(), decoder.getFileName());
							dataReceiver.sendResponse(decoder.getServerResponse());
						} catch (DataTransferException e) {
							LOGGER.error("Data received from a client is not consistent.");
						}
					}

				}).start();
			}

		}
	}

}
