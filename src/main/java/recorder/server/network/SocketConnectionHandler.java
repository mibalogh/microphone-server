package recorder.server.network;

import java.io.IOException;
import java.net.ServerSocket;

import org.apache.log4j.Logger;

public class SocketConnectionHandler extends ServerSocket implements IConnectionHandler {
	
	private final static Logger LOGGER = Logger.getLogger(SocketConnectionHandler.class);

	public SocketConnectionHandler(int port) throws IOException {
		super(port);
	}

	@Override
	public IReceiver listenForConnection() {
		try {
			LOGGER.info("Waiting for connection.");
			SocketDataReceiver connectionSocket = new SocketDataReceiver(accept());
			LOGGER.info("Client Connected.");
			return connectionSocket;
		} catch (IOException e) {
			LOGGER.info("Listening for connection stopped. Socket was closed.");
		}
		return null;
	}

	@Override
	public void closeConnection() {
		try {
			close();
		} catch(IOException e) {
			LOGGER.info("Listening for connection stopped. Socket was closed.");
		}
	}
	

}
