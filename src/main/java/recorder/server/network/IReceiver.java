package recorder.server.network;

public interface IReceiver {
	
	byte[] readData();
	
	void saveDataToFile(byte[] data, String filename);
	
	void sendResponse(byte[] response);
	
}
