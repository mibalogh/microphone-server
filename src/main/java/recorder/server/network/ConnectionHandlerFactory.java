package recorder.server.network;

import java.io.IOException;

import org.apache.log4j.Logger;

import recorder.server.Configuration;

public class ConnectionHandlerFactory {
	
	private final static Logger LOGGER = Logger.getLogger(ConnectionHandlerFactory.class);

	public static IConnectionHandler getConnectionHandler() {
		try {
			return new SocketConnectionHandler(Configuration.getPort());
		} catch (IOException e) {
			LOGGER.error("Could not initialize connection listener. Check network configuration and port availability.");
		}
		return null;
	}
}
