package recorder.server.network;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

import recorder.server.Configuration;

public class SocketDataReceiver implements IReceiver {
	
	private final static Logger LOGGER = Logger.getLogger(SocketDataReceiver.class);
	
	private static final int BUFFER_SIZE = 1024;
	
	private static final int EOF = -1;

	private Socket mSocket;
	
	public SocketDataReceiver(Socket socket) throws IOException {
		mSocket = socket;
	}

	@Override
	public byte[] readData() {
		try {
			InputStream inputStream = mSocket.getInputStream();
			ByteArrayOutputStream byteArrayOuputStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[BUFFER_SIZE];
			int lengthBytesRead;
			while((lengthBytesRead = inputStream.read(buffer, 0, BUFFER_SIZE)) != EOF) {
				byteArrayOuputStream.write(buffer, 0, lengthBytesRead);
			}
			return byteArrayOuputStream.toByteArray();
		} catch (IOException e) {
			LOGGER.error("Could not read the data from stream.");
		}
		return null;
	}

	@Override
	public void saveDataToFile(byte[] data, String filename) {
		try {
			Path path = Paths.get(Configuration.getUserHome(), filename);
			Path parentPath = path.getParent();
			if(parentPath != null && Files.notExists(parentPath)) {
				Files.createDirectories(parentPath);
			}
			Files.write(path, data);
			LOGGER.info("File " + filename + " saved successfully.");
		} catch (FileNotFoundException e) {
			LOGGER.error("Could not write a file. File with that name does not exists.");
		} catch (IOException e) {
			LOGGER.error("Could not write to the filestream.");
		}
		
	}

	@Override
	public void sendResponse(byte[] response) {
		try {
			OutputStream outputStream = mSocket.getOutputStream();
			outputStream.write(response);
		} catch (IOException e) {
			LOGGER.error("Could not write the answer to the stream.");
		} finally {
			try {
				mSocket.close();
			} catch (IOException e) {
				LOGGER.error("Could not close the socket.");
			}
		}
		
		
	}


}
