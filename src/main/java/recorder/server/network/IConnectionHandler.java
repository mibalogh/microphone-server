package recorder.server.network;

public interface IConnectionHandler {

	IReceiver listenForConnection();
	
	void closeConnection();
}
